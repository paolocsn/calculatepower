<?php

namespace App\Services;


class Calculations
{
    /**
     * @var $hull_length
     */
    protected $hull_length;

    /**
     * @var $buttock_angle
     * Range between 2° and -7°
     */
    private $buttock_angle;

    /**
     * @var $displacement
     */
    private $displacement;


    /**
     * CalculatePower constructor.
     *
     * @param $hull_length
     * @param $buttock_angle
     * @param $displacement
     */
    public function __construct($hull_length, $buttock_angle, $displacement)
    {
        $this->hull_length = $hull_length;
        $this->buttock_angle = $buttock_angle;
        $this->displacement = $displacement;

    }

    /**
     * @return float|int
     */
    public function calculateHorsePower()
    {
        $sl_ratio = $this->calculateSLRatio();
        $hull_speed = $this->calculateHullSpeed($sl_ratio);
        $cw = $this->calculateCW($sl_ratio);

        $horse_power = ($this->displacement / 1000) * ($cw * pow(pow($this->hull_length,1/2), 3));

        return $horse_power;

    }

    /**
     * @return float
     */
    public function calculateSLRatio()
    {
        $sl_ratio = ($this->buttock_angle * -0.2) + 2.9;
        return $sl_ratio;

    }

    /**
     * Calculation is in knots.
     *
     * @param $sl_speed_ratio
     * @return float|int
     */
    public function calculateHullSpeed($sl_speed_ratio)
    {
        $hull_speed = $sl_speed_ratio * pow($this->hull_length, 1/2);
        return $hull_speed;

    }

    /**
     * @param $sl_ratio
     * @return float
     */
    public function calculateCW($sl_ratio)
    {
        $cv = 0.8 + (0.17 * $sl_ratio);
        return $cv;

    }

}
