<?php

namespace App\Http\Controllers;

use App\Services\Calculations;
use Illuminate\Http\Request;


class CalculateController extends Controller
{

    /**
     * Define validation rules.
     */
    protected $validationRules = [
        'hull_length' => 'bail|required|numeric|min:-7|max:2',
        'buttocks' => 'required|numeric',
        'displacement' => 'required|numeric',
    ];

    /**
     * Show the profile for the given user.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view()
    {
        $name = 'Calculate';

        return view('calculate.index', compact('name'));

    }

    /**
     * Process the request, validate input and calculate.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function process(Request $request)
    {

        $validatedData = $request->validate($this->validationRules);

        $calculations = new Calculations($validatedData['hull_length'], $validatedData['buttocks'], $validatedData['displacement']);
        $sl_ratio = $calculations->calculateSLRatio();
        $hull_speed = $calculations->calculateHullSpeed($sl_ratio);
        $boat_power = $calculations->calculateHorsePower();

        //dd('validate data: ', $validatedData, 'boat_power: ', $boat_power);

        return back()->with('success', 'The horse power required is ' . $boat_power . ' horses - The hull speed is ' . $hull_speed . ' knots');

    }


}

