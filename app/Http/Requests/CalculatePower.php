<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CalculatePower extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hull_length' => 'required|digits|max:5',
            'buttocks' => 'required|digits|max:2|min:-7',
            'displacement' => 'required|digits|max:5',

        ];
    }
}
