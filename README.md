# CalculatePower
## General comments
This is an simple implementation of the exercises.
Because I did not have all the instructions e.g. about conversions feet to libs to horse power, the calculations are applied at face value.
I have used the Laravel framework to make it quicket (it is what I have used in my last project), but given more time I can do the same with Symfony.

## Architecture
These are the location of the main files:

- presentation
resources\views\calculate\index.blade.php

- Controller
app\Http\Controllers\CalculateController.php

- Logic
app\Services\Calculations.php


## Installation
Basic installation of Laravel:

*composer global require laravel/installer*

*laravel new Test*

Run with local server:

*php artisan serve*

Ref:
[https://laravel.com/docs/5.7/installation](a href="https://laravel.com/docs/5.7/installation)

# Laraval
## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

