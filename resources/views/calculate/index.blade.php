@extends('layouts.app')

@section('title', 'Sony ATV test')

@section('sidebar')
    @parent

    <p>Sidebar</p>
@endsection

@section('content')

    <div class="">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (\Session::get('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif

        <p><b>Power requirements for boat </b></p>

        <div class="form-group">

            {!! Form::open(array('action' => 'CalculateController@process', 'method'=>'POST'))  !!}

            {!! csrf_field() !!}

            <label for="hull_length">Hull length:</label>
            {!! Form::input('hull_length', 'hull_length') !!}<br><br>

            <label for="buttocks">Buttocks angle:</label>
             {!! Form::input('buttocks', 'buttocks') !!}<br><br>

            <label for="displacement">Displacement:</label>
            {!! Form::input('displacement', 'displacement') !!}<br><br>

            {!! Form::submit('Calculate') !!}

            {!! Form::hidden('') !!}

            {!! Form::close() !!}

        </div>
        <br/>
        <p>* Power requirements. </p>

        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif

    </div>

@endsection